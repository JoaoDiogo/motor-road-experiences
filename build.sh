#!/bin/sh

git pull

pm2 stop all

cd _api
yarn
cd ..

cd site
yarn
yarn build
cd ..
cd admin
yarn
yarn build
cd ..

pm2 start all
