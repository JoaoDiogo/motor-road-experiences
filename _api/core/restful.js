const mongoose = require('mongoose')

module.exports = class Restful {
  constructor (app, baseUri) {
    this.model = mongoose.model(baseUri.split("/").pop())
    this.baseUri = baseUri
    this.app = app
    return this
  }

  c () {
    /**
     * PUT http://{server}/{baseURI}
     * Create new document
     * */ 
    this.app.put(`${this.baseUri}`, async (req, res, next) => {
      try {
          let r = new this.model(req.body)
          await r.save()
          if(r._id) {
              res.json({ status: 'OK', registers: r })
              next()
          }
          else
              res.json({ status: 'FAIL' })
      } catch (err) {
          res.json({ status: "FAIL", err })
      }
    })

    return this
  }

  r ({ populate, uri }) {
    /**
     * GET http://{server}/{baseURI}
     * Get document collection
     * Supports pagging, sort, filter
     */
    this.app.get(`${this.baseUri}${uri ? `-${uri}` : ''}`, async (req, res, next) => {
      try {
          // convert url parameters into filters
          let q = {}
          if(req.query.q) {
            q = req.query.q.split(',').map(item => {
              let terms = item.split(':')
              
              if(terms[0].endsWith('[regex]')) {
                return {[terms[0].replace('[regex]', '')]: new RegExp(`${terms[1]}`, 'gi') }
              } else {
                return {[terms[0]]: terms[1]}
              }
            })

            q = { $and: q }
          }

          // cria projection
          let projection = {}
          if(req.query.fields) {
            req.query.fields.split(',').map(item => {
              projection[item] = 1
            })
          }

          let c = await this.model.countDocuments(q)

          let r = await this.model.find(q, projection)
            .skip(req.query.skip ? parseInt(req.query.skip) : 0)
            .limit(req.query.limit ? parseInt(req.query.limit) : 0)
            .sort(req.query.sort ? req.query.sort.replace(/,/g, ' ') : '')
            .populate(populate)
            .lean()

          res.json({ status: 'OK', count: c, registers: r })
          next()
      } catch (err) {
          res.json({ status: 'FAIL', err })
      }
    })

    /**
     * GET http://{server}/{baseURI}/:id
     * Get single document by id
     */
    this.app.get(`${this.baseUri}/:id${uri ? `/${uri}` : ''}`, async (req, res, next) => {
      try {
          let projection = {}

          if(req.query.fields) {
            req.query.fields.split(',').map(item => {
              projection[item] = 1
            })
          }

          let r = await this.model.findOne({ _id: req.params.id }, projection).populate(populate).lean()
          res.json({ status: 'OK', registers: r })
          next()
      } catch (err) {
          res.json({ status: 'FAIL', err })
      }
    })

    return this
  }

  u () {
    /**
     * POST http://{server}/{baseURI}/:id
     * Update single document by id
     */
    this.app.post(`${this.baseUri}/:id`, async (req, res, next) => {
      try {
          let r = await this.model.findOne({ _id: req.params.id })

          // control fields remove
          delete(req.body._id)
          delete(req.body.__v)

          for(let key in req.body) { r[ key ] = req.body[ key ] }
          r.save()
          res.json({ status: "OK" })
          next()
      } catch (err) {
          res.json({ status: "FAIL", err })
      }
    })
    return this
  }

  d () {
    /**
     * DELETE http://{server}/{baseURI}/:id
     * Delete single document by id
     */
    this.app.delete(`${this.baseUri}/:id`, async (req, res, next) => {
      try {
          let r = await this.model.findOne({ _id: req.params.id })
          r.remove()
          res.json({ status: "OK" })
          next()
      } catch ( err ) {
          res.json({ status: "FAIL", err })
      }
    })
    return this
  }

  count () {
    /**
     * GET http://{server}/{baseURI}:count
     * Count documents
     */
    this.app.get(`${this.baseUri}/count`, async (req, res, next) => {
      // convert url parameters into filters
      let q = {}
      if(req.query.q) {
        q = req.query.q.split(',').map(item => {
          let terms = item.split(':')
          
          if(terms[0].endsWith('[regex]')) {
            return {[terms[0].replace('[regex]', '')]: new RegExp(`${terms[1]}`, 'gi') }
          } else {
            return {[terms[0]]: terms[1]}
          }
        })

        q = { $and: q }
      }

      try {
          let r = await this.model.countDocuments(q)
          res.json({ status: "OK", count: r })
          // next()
      } catch ( err ) {
          res.json({ status: "FAIL", err })
      }
    })
    return this
  }

  full (populate) {
    this.count().c().r({ populate }).u().d()
    return this
  }
}
