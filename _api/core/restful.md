# Modelo de uso do component restful

## Mongoose Model
``` js
const mongoose = require('mongoose')

mongoose.model('city', {
    name: String,
    uf: String
})

mongoose.model('register', {
    active: Boolean,
    name: String,
    login: String,
    city: {
        mongoose.Types.ObjectId,
        ref: 'city'
    }
})

mongoose.model('relation', {
    type: {
        type: String,
        enum: ['match', 'block']
    },
    ref: {
        type: mongoose.Types.ObjectId,
        ref: 'register'
    }
})

```

## Exemplo de controller
``` js
const restful = requie('./restful')

/**
 * ativa modelo restful somente com dados da collection
 * GET      - http://{server}/api/city          - lista de cidades (suporta filtros, ordenação e paginação)
 * GET      - http://{server}/api/city/:id      - cidade única (id)
 * GET      - http://{server}/api/city/count    - contagem de registros (suporta filtros)
 * PUT      - http://{server}/api/city/:id      - nova cidade
 * POST     - http://{server}/api/city          - atualizar cidade
 * DELETE   - http://{server}/api/city/:id      - excluir cidade
 */
new restful(app, '/api/city').full()

/**
 * ativa modelo restful com um nivel de dados (relação com segunda collection)
 * GET      - http://{server}/api/register
 * GET      - http://{server}/api/register/:id
 * GET      - http://{server}/api/register/count
 * PUT      - http://{server}/api/register/:id
 * POST     - http://{server}/api/register
 * DELETE   - http://{server}/api/register/:id
 */
new restful(app, '/api/register').full(['city'])

/**
 * ativa modelo restful com dois níveis de dados
 * GET      - http://{server}/api/relation
 * GET      - http://{server}/api/relation/:id
 * GET      = http://{server}/api/relation/count
 * PUT      - http://{server}/api/relation/:id
 * POST     - http://{server}/api/relation
 * DELETE   - http://{server}/api/relation/:id
 */
new restful(app, '/api/relation').full([
    {
        path: 'ref',
        select: 'name login',
        populate: {
            path: 'city',
            select: 'name uf'
        }
    }
])

/**
 * cria rota simplificada - popula referência da cidade
 * GET      - http://{server}/api/customer-with-city
 * GET      - http://{server}/api/customer/:id/with-city
 */
new restful(app, '/api/customer')
    .r({ populate: ['city'], uri: '/with-city' })

/**
 * modelo restful com rota adicional - popula referência da cidade
 * GET      - http://{server}/api/customer
 * GET      - http://{server}/api/customer/:id
 * GET      - http://{server}/api/customer/count
 * PUT      - http://{server}/api/customer/:id
 * POST     - http://{server}/api/customer
 * DELETE   - http://{server}/api/customer/:id
 * GET      - http://{server}/api/customer-with-city
 * GET      - http://{server}/api/customer/:id/with-city
 */
new restful(app, '/api/customer')
    .full()
    .r({ populate: ['city'], uri: '/with-city' })

/**
 * Modelo restful
 * 
 * Chamadas de listagem podem conter parametros
 * skip (numerico)  - salta um determinado numero de registros
 * limit (numerico) - limita um determinado numero de registros 
 * sort (array)     - ordena listagem
 * q (string)       - filtragem
 * fields (array)   - seleção de campos do select
 * 
 * GET      - http://{server}/api/customer
 * GET      - http://{server}/api/customer?skip=10&limit=10&sort=-name,active
 * GET      - http://{server}/api/customer?q=title[regex]:teste,active:true
 * GET      - http://{server}/api/customer?fields=name,login
 * GET      - http://{server}/api/customer/count
 * GET      - http://{server}/api/customer/:id
 * GET      - http://{server}/api/customer/:id/exists - não implantado
 * PUT      - http://{server}/api/customer/:id
 * POST     - http://{server}/api/customer
 * DELETE   - http://{server}/api/customer/:id
 * GET      - http://{server}/api/customer-with-city
 * GET      - http://{server}/api/customer/:id/with-city
 */
new restful(app, '/api/customer')
    .full()
    .r({ populate: ['city'], uri: '/with-city' })

```
# Funções não implantadas
- rules
