const mongoose = require('mongoose')
const notification = mongoose.model('notification')
const notify = mongoose.model('notify')
const register = mongoose.model('register')
const fs = require('fs')
const nodemailer = require('nodemailer')

var transporter = nodemailer.createTransport({
    host: 'smtp.zoho.com',
    port: 465,
    secure: true,
    auth: {
        user: process.env.EMAIL,
        pass: process.env.EMAIL_PASS
    }
})

const emailModel = `
<table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td style="font-family:tahoma,arial,verdana; font-size:12px; padding-left:10px; color: #000;">
            #content#
            <br />
        </td>
    </tr>
    <tr>
        <td>
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
            <td style="padding: 5px;"><img src="cid:logo"></td>
            <td style="font-family:tahoma,arial,verdana; font-size:11px; padding-left:8px;  padding-top:1px; valign="top">
                <strong>Equipe Dossiê Saúde</strong>
                <br>
                <a href="${process.env.SITE_URL}" target="_blank">${process.env.SITE_URL}</a>
                <br>
            </td>
            </tr>
        </table>
        </td>
    </tr>
</table>
`
module.exports = {
    async warning(data) {
        var mailOptions = {
            from: `${process.env.CORPORATION} <${process.env.EMAIL}>`,
            to: process.env.EMAIL,
            subject: 'Mensagem enviado pelo site.',
            html: data,
            attachments: []
        }
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                return false
            } else {
                return true
            }
        })
    },
    async sendExternal(core, _id, key, data) {

        let r = await notification.findOne({ key })
        if (r && r.emailActive) {

            const logo = fs.readFileSync(__dirname + '/images/dossie-saude.png', { encoding: 'base64' })
            let rg = await register.findById(_id)

            var mailOptions = {
                from: `${process.env.CORPORATION} <${process.env.EMAIL}>`,
                to: data,
                subject: r.emailSubject.replace(/#name#/g, rg.name),
                html: emailModel.replace('#content#', r.emailText.replace(/#link#/g, `${process.env.SITE_URL}/user/${rg.hash}`)).replace(/#name#/g, rg.name),
                attachments: [
                    {
                        filename: process.env.EMAIL_IMAGE,
                        path: `${__dirname}/images/${process.env.EMAIL_IMAGE}`,
                        cid: 'logo'
                    }
                ]

            }

            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    return false
                } else {
                    return true
                }
            })
        }
    },

    async send(core, _id, key, data) {

        let r = await notification.findOne({ key })

        if (r && r.notifyActive) {
            let rg = await register.findById({ _id }, { name: 1 })
            let n = new notify({ to: _id, message: r.notifyText.replace(/#data#/g, data).replace(/#name#/g, rg.name) })
            n.save()

            let count = await notify.countDocuments({ to: _id })
            core.io.to(_id).emit('notify', { count })
        }

        if (r && r.emailActive) {

            const logo = fs.readFileSync(`${__dirname}/images/${process.env.EMAIL_IMAGE}`, { encoding: 'base64' })
            let rg = await register.findById(_id)

            var mailOptions = {
                from: `${process.env.CORPORATION} <${process.env.EMAIL}>`,
                to: rg.email,
                subject: r.emailSubject,
                html: emailModel.replace('#content#', r.emailText.replace(/#link#/g, data).replace(/#name#/g, rg.name).replace(/#password#/g, data).replace(/#corporation#/g, process.env.CORPOTATION)),
                attachments: [
                    {
                        filename: process.env.EMAIL_IMAGE,
                        path: `${__dirname}/images/${process.env.EMAIL_IMAGE}`,
                        cid: 'logo'
                    }
                ]
            }

            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    return false
                } else {
                    return true
                }
            })
        }
    },

    async service(core, _id, key, data) {
        //console.log('Servuce: ', _id, key, data)
        let r = await notification.findOne({ key })
        //console.log(r)
        if (r && r.notifyActive) {

            let icon = ''
            switch (key) {
                case "notify-drug":
                    icon = 'mdi-pill'
                    break;
                case "welcome-patient":
                    icon = 'mdi-flower-tulip'
                    break;
                case "welcome-professional":
                    icon = 'mdi-flower-tulip'
                    break;
                case "welcome-manager":
                    icon = 'mdi-flower-tulip'
                    break;
                default:
                    icon = 'mdi-alert-circle-outline'
            }

            let n = new notify({ to: _id, icon: icon, message: r.notifyText.replace(/#data#/g, data).replace(/#link#/g, process.env.SITE_URL) })
            n.save()

            let count = await notify.countDocuments({ to: _id })
            core.io.to(_id).emit('notify', { count })

        }

    }
}