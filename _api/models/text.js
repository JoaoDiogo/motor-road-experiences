const mongoose = require('mongoose')

mongoose.model('text', {
    type: String,
    title: String,
    subject: String,
    content: String
})