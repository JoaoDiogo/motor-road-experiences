const mongoose = require('mongoose')

mongoose.model('event', {
    hash: String,
    day: Date,
    time: String,
    type: String,
    title: String,
    subtitle: String,
    content: String,
    status: {
        type: String,
        default: 'pending',
        required: true,
        enum: [
            'approved',
            'pending'
        ],
    },
})

async function make() {
    const auth = mongoose.model('event')
    if(await auth.countDocuments() === 0) {
        const created = new auth({
            hash: 'ASDF@DAS$',
            day: "2021-06-27 00:00:00.407Z",
            time: "90:00:00",
            type: "Festa",
            title: "Festa de 1° ANO",
            subtitle: "Agora sim",
            content: "CONTEUDO",
            status: "approved"
        })

        created.save()
    }
}

make()