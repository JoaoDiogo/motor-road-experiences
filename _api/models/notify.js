const mongoose = require('mongoose')

mongoose.model('notify', {
    to: mongoose.Types.ObjectId,
    icon: String,
    message: String,
    unread: {
        type: Boolean,
        default: true
    },
    created: {
        type: Date,
        default: Date.now
    }
})