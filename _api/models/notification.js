const mongoose = require('mongoose')

mongoose.model('notification', {
    key: {
        type: String
    },

    title: String,

    notifyActive: Boolean,
    notifyText: String,

    emailActive: Boolean,
    emailSubject: String,
    emailText: String
})
