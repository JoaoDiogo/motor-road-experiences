const mongoose = require('mongoose')
const register = {
    name: String,
    nickname: String,
    email: {
        type: String,
        unique: true,
        lowercase: true,
        trim: true
    },
    status: {
        type: String,
        default: 'approved',
        required: true,
        enum: [
            'account-confirm',
            'approved',
            'approval',
            'disapproved',
            'pending',
            'blocked',
            'account-remove',
            'banned'
        ],
    },
    role: {
        type: String,
        required: true,
        enum: ['user', 'partner']
    },
    gender: {
        type: String,
        enum: ['Masculino', 'Feminino'],
    },
    avatar: {
        type: String,
        default: ""
    },
    birthdate: Date,
    password: { type: String, select: false },
    hash: String,
    created: { type: Date, default: Date.now },
    lastLogin: Date,
    removed: Date,
}
mongoose.model('register', register)
module.exports = mongoose