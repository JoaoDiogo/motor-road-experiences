const mongoose = require('mongoose')

mongoose.model('auth', {
    name: {
        type: String,
        required: true
    },

    password: {
        type: String,
        required: true
    },

    email: {
        type: String,
        required: true
    },

    lastLogin: {
        type: Date
    }
})

async function makeAdminUser() {
    const auth = mongoose.model('auth')
    if(await auth.countDocuments() === 0) {
        const adminUser = new auth({
            name: 'admin',
            email: 'admin@ginge.com.br',
            password: 'IAQ93Nre'
        })

        adminUser.save()
    }
}

makeAdminUser()