
/* const register = require('./factory/register');

test('Insert Register: ', () => {
    expect(register({name: "Joao", role: "scholl"}));
}); */

const mongoose = require("mongoose");

(async function(){
    require("./models/register")
    const register = require("./factory/register")()

    await mongoose.connect(
        'mongodb://127.0.0.1/skillTraining',
        {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            //autoReconnect: true,
            //reconnectInterval: 1000,
            //reconnectTries: 30,
            poolSize: 10,
            keepAlive: true, 
            keepAliveInitialDelay: 300000,
            useCreateIndex: true,
            useFindAndModify: false
        }
    )

    await register.insert()
    console.log(register)

//{name: "Joao", role: "scholl"}    
    mongoose.disconnect()
})()





// console.log(new Date(Date.now()).toLocaleString())