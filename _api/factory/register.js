const mongoose = require("mongoose")
require('../models/register')
const model = mongoose.model('register')
const b64 = require('base64-img')
const fs = require('fs')
const QRCode = require('qrcode')
const generator = require('generate-password')
const bcrypt = require('bcryptjs')

module.exports = () => {

    async function insert(data) {
        try {
            let register = new model(data)
            return register.save()
        } catch {
            return 'FAIL'
        }
    }

    async function createProfileFolder(obj, hash) {
        try {
            if (!fs.existsSync(`../site/static/profiles/${obj._id}`)) {
                fs.mkdirSync(`../site/static/profiles/${obj._id}`)
                fs.mkdirSync(`../site/static/profiles/${obj._id}/image`)
                // ORCode
                let url = `${process.env.SITE_URL}/user/${hash}`
                const image = await QRCode.toDataURL(url)
                let file = b64.imgSync(image, `../site/static/profiles/${obj._id}/image/`, hash)
                return 'OK'
            }
            return null
        } catch {
            return 'FAIL'
        }
    }

    async function createPassword(obj, password) {
        try {
            if (obj !== "" && password === "") {
                let r = await model.findOne({ _id: obj }, { password: 1 })
                password = generator.generate({
                    length: 10,
                    numbers: true,
                    lowercase: true,
                    uppercase: true,
                    strict: true
                })
                var salt = bcrypt.genSaltSync(10);
                var pass = bcrypt.hashSync(password, salt);
                r.password = pass
                r.save()
                return password
            } else if (obj === "" && password !== "") {
                var salt = bcrypt.genSaltSync(10);
                var pass = bcrypt.hashSync(password, salt);
                return pass
            } else if (obj !== "" && password !== "") {
                let r = await model.findOne({ _id: obj }, { password: 1 })
                var salt = bcrypt.genSaltSync(10);
                var pass = bcrypt.hashSync(password, salt);
                r.password = pass
                return r.save()
            }
        } catch {
            return 'FAIL'
        }
    }

    async function updateAccount(data) {
        try {
            let r = await model.findOne({ _id: data._id })
            if (r) {
                r.name = data.name
                r.gender = data.gender
                r.cpf = data.cpf
                r.birthdate = data.birthdateNew
                r.save()
                return 'OK'
            } else {
                return 'FAIL'
            }
        } catch {
            return 'FAIL'
        }
    }

    async function removeAccount(obj) {
        try {
            let r = await model.findOne({ _id: obj })
            if (r) {
                r.status = 'account-remove'
                r.save()
                return 'OK'
            } else {
                return 'FAIL'
            }
        } catch {
            return 'FAIL'
        }
    }

    return {
        insert,
        createProfileFolder,
        createPassword,
        updateAccount,
        removeAccount
    }

}