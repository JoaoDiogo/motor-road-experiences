require('dotenv').config()

const express = require('express')
const loader = require('express-load')
const compression = require('compression')
const cors = require('cors')
const mongoose = require('mongoose')
const fs = require('fs')
const helmet = require('helmet')

// Core Modules
const restful = require('./core/restful')

mongoose.connect(
    'mongodb://127.0.0.1/motorRomeExperience',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        // autoReconnect: true,
        // reconnectInterval: 1000,
        // reconnectTries: 30,
        poolSize: 10,
        keepAlive: true,
        keepAliveInitialDelay: 300000,
        useCreateIndex: true,
        useFindAndModify: false
    }
)

const app = express()

// Socket.io config
const http = require('http').Server(app)
const io = require('socket.io')(http, {
    cors: {
        origin: process.env.SITE_URL,
        methods: ["GET", "POST"],
        allowedHeaders: ["my-custom-header"],
        credentials: true
    }
})

io.of('/:2000')

app.use(express.urlencoded({ extended: true, limit: '50mb' }))
app.use(express.json({ limit: '50mb' }))
app.use(compression())
app.use(helmet())
app.use(cors())

loader('models')
    .then('controllers')
    .into({ app, core: { restful, io } })

http.listen(process.env.PORT, () => {
    console.log(`Server listen on port ${process.env.PORT}`)
})