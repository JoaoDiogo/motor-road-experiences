const mongoose = require('mongoose')

const notify = require('../lib/notify')

module.exports = ({ app, core }) => {
    app.get('/api/notify/by-user', async (req, res) => {
        let notify = mongoose.model('notify')
        
        let r = await notify.find({ to: req.auth._id }).sort({created: -1})
        res.json({ status: 'OK', registers: r })
    })

    app.get('/api/notify/by-user/readed', async (req, res) => {
        let notify = mongoose.model('notify')
        let r = await notify.updateMany({ to: req.auth._id }, { unread: false })
        res.json({ status: 'OK', nModified: r.nModified})
    })

    app.post('/api/notify/complaint', async (req, res) => {
        try {
            notify.send(core, req.body._id, 'complaint', '')
            res.json({ status: 'OK' })
        }
        catch {
            res.json({ status: 'FAIL' })
        }
    })
    
    app.post('/api/notify/user', async (req, res) => {
        try {
            console.log('req: ', req.body.key)
            notify.service(core, req.body._id, req.body.key, req.body.data)
            res.json({ status: 'OK' })
        }
        catch {
            res.json({ status: 'FAIL' })
        }
    })

    //Remove notify by-user  
    app.delete('/api/notify/by-user', async (req, res) => {
        if(req.auth._id){
            try {
                let notify = mongoose.model('notify')
                let r = await notify.find({ to: req.auth._id })
                r.map( e => e.remove())
                res.json({ status: 'OK' })
            }
            catch {
                res.json({ status: 'FAIL' })
            }
        }else{
            res.json({ status: 'FAIL', message: 'Disconnected user' })
        }
        
    })
    
    //Remove notify :id   
    app.delete('/api/notify/:id', async (req, res) => {
        if(req.auth._id){
            try {
                let notify = mongoose.model('notify')
                let r = await notify.findOne({ _id: req.params.id })
                r.remove()
                res.json({ status: 'OK' })
            }
            catch {
                res.json({ status: 'FAIL' })
            }
        }else{
            res.json({ status: 'FAIL', message: 'Disconnected user' })
        }
    })

    new core.restful(app, '/api/notify')
    .full()
}