const mongoose = require('mongoose')
const model = mongoose.model('register')
const bcrypt = require('bcryptjs')
const factory = require("../factory/register")()
const notify = require('../lib/notify')
const jwt = require("jsonwebtoken")
const generator = require('generate-password')

module.exports = ({ app, core }) => {

    app.put('/api/signup', async (req, res) => {
        try {
            let hash = Math.random().toString(36).toUpperCase().slice(-10)
            let obj = { ...req.body }
            obj.hash = hash
            obj.password = await factory.createPassword("", req.body.password)
            let o = await factory.insert(obj)
            if (o._id) {
                factory.createProfileFolder(o._id, obj.hash)
                token = await jwt.sign({ _id: o._id }, process.env.TOKEN_SECRET, { expiresIn: '30m' })
                notify.send(core, o._id, 'welcome', '')
                res.json(token)
            }

        } catch {
            res.json({ status: 'FAIL' })
        }
    })

    app.get('/api/signup/:email/forgot', async (req, res) => {
        let r = await model.findOne({ email: req.params.email }, { _id: 1 })
        if (r) {
            let pass = await factory.createPassword(r._id, '')
            if(pass!=="FAIL"){
                notify.send(core, r._id, 'forgotten-password', pass)
                res.json({ status: 'OK' })
            }else{
                res.json({ status: 'FAIL' })
            }
        } else {
            res.json({ status: 'FAIL' })
        }
    })

    app.post('/api/register/updatePassword', async (req, res) => {
        if(req.auth){
            console.log(req.auth._id, req.body.new)
            if(await factory.createPassword(req.auth._id, req.body.new)!=="FAIL"){
                res.json({ status: 'OK' })
            }else{
                res.json({ status: 'FAIL' })
            }
        }else {
            res.json({ status: 'FAIL' })
        }
    })

    // Dados gerais
    app.post('/api/register/update', async (req, res) => {
        if (req.auth) {
            try {
                if (await factory.updateAccount(req.body) === "OK") {
                    res.json({ status: 'OK' })
                } else {
                    res.json({ status: 'FAIL' })
                }
            } catch (error) {
                res.json({ status: 'FAIL' })
            }
        } else {
            res.json({ status: 'FAIL' })
        }
    })

    // Dados account-remove 
    app.post('/api/register/account-remove', async (req, res) => {
        if (req.auth) {
            try {
                if (await factory.removeAccount(req.body._id) === "OK") {
                    res.json({ status: 'OK' })
                } else {
                    res.json({ status: 'FAIL' })
                }
            } catch (error) {
                res.json({ status: 'FAIL' })
            }
        } else {
            res.json({ status: 'FAIL' })
        }
    })

    app.get('/api/register/:hash/extends', async (req, res) => {
        let r = await model.findOne({ _id: req.params.hash })
        if (r) {
            res.json(r)
        } else {
            res.json({ status: 'FAIL' })
        }
    })

    new core.restful(app, '/api/register')
        // Cria Restful base
        .full()

    // SOCKET
    core.io.on('connection', socket => {
        // Register user on web socket
        socket.emit('register', socket.id)
        socket.on('handshake', async payload => {
            if (payload !== null) {
                // console.log(`user register ${payload}`)
                socket._id = payload
                socket.join(payload)
                core.io.emit('on-line', payload)

                // Coleta total de notificações não lidas do usuário
                let notify = mongoose.model('notify')
                let countNotify = await notify.countDocuments({ to: socket._id, unread: true })
                core.io.to(socket._id).emit('notify', { count: countNotify })
            }
        })

        socket.on('disconnect', () => {
            core.io.emit('off-line', socket._id)
            console.log('disconnected', socket._id)
        })
    })

}