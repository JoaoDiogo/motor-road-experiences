const mongoose = require('mongoose')
const model = mongoose.model('text')


module.exports = ({ app, core }) => {

    // Termo de uso
    app.get('/api/text/terms', async (req, res) => {
        let r = await model.findOne({ type: 'terms' }, { content: 1 })
        if (r) {
            res.json({ status: 'OK', content: r.content })
        } else {
            res.json({ status: 'FAIL' })
        }
    })

    // Política de privacidade
    app.get('/api/text/policy', async (req, res) => {
        let r = await model.findOne({ type: 'policy' }, { content: 1 })
        if (r) {
            res.json({ status: 'OK', content: r.content })
        } else {
            res.json({ status: 'FAIL' })
        }
    })

    // Sobre a Empresa
    app.get('/api/text/about', async (req, res) => {
        let r = await model.findOne({ type: 'about' }, { content: 1 })
        if (r) {
            res.json({ status: 'OK', content: r.content })
        } else {
            res.json({ status: 'FAIL' })
        }
    })

    new core.restful(app, '/api/text')
        // Cria Resful base
        .full()
}
