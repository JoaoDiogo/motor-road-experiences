const mongoose = require('mongoose')
const model = mongoose.model('register')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')

module.exports = ({ app, core }) => {
    app.use((req, res, next) => {
        try {
            let token = req.headers['x-access-token'] || req.headers['authorization']

            if (token.startsWith('Bearer'))
                token = token.slice(7, token.length)

            if (token) {
                jwt.verify(token, process.env.TOKEN_SECRET, async (err, decoded) => {
                    if (!err) {
                        req.auth = decoded
                    }
                })
            }

            next()
        }
        catch {
            next()
        }
    })

    app.use("/api/auth/signin", async (req, res) => {
        try {
            let r = await model.findOne({ $or: [{ email: req.body.user }], status: { $in: ['approved', 'approval', 'pending'] } }, { _id: 1, email: 1, password: 1 })
            return bcrypt.compare(req.body.passwd, r.password, function (err, isMatched) {
                if (isMatched === true) {
                    let token = jwt.sign({ _id: r._id, email: r.email }, process.env.TOKEN_SECRET, { expiresIn: '30m' })
                    res.json({ token })
                } else {
                    console.log(err)
                    res.json({ status: 'FAIL' })
                }
            });
        } catch {
            res.json({ status: 'FAIL' })
        }
    })

    app.post("/api/auth/refresh", async (req, res) => {
        console.log('refresh')
        if (req.auth) {
            try {
                let token = jwt.sign({ _id: req.auth._id, email: req.auth.email }, process.env.TOKEN_SECRET, { expiresIn: '30m' })
                res.json({ token })
            } catch (err) {
                res.json({ status: 'FAIL' })
            }
        } else {
            console.log('Refresh Fail: Token expirado')
            res.json({ status: 'FAIL' })
        }
    })

    app.use("/api/auth/admin/signin", async (req, res) => {
        const auth = mongoose.model('auth')

        try {
            if (req.body.user !== '' && req.body.password !== '') {
                let regex = new RegExp(req.body.user, "i")
                let r = await auth.findOne({ $or: [{ email: req.body.user }, { name: regex }] }, { _id: 1, email: 1, password: 1 })
                if (r.password === req.body.passwd) {
                    let token = jwt.sign({ _id: r._id, email: r.email }, process.env.TOKEN_SECRET, { expiresIn: '30m' })
                    res.json({ token })
                } else {
                    res.json({ status: 'FAIL' })
                }
            } else {
                res.json({ status: 'FAIL' })
            }
        } catch {
            res.json({ status: 'FAIL' })
        }
    })

    app.post("/api/auth/admin/refresh", async (req, res) => {
        if (req.auth) {
            try {
                let token = jwt.sign({ _id: req.auth._id, email: req.auth.email }, process.env.TOKEN_SECRET, { expiresIn: '30m' })
                res.json({ token })
            } catch (err) {
                res.json({ status: 'FAIL' })
            }
        } else {
            console.log('Refresh Fail: Token expirado')
            res.json({ status: 'FAIL' })
        }
    })

    app.get("/api/auth/admin/profile", async (req, res) => {
        const auth = mongoose.model('auth')
        if (req.auth) {
            try {
                let r = await auth.findOne({ _id: req.auth._id }, 'email name lastLogin')
                r.lastLogin = Date.now()
                r.save()

                res.json({ ...r, role: 'admin' })
            } catch {
                res.json({ status: 'FAIL' })
            }
        } else {
            res.json({ status: 'FAIL' })
        }
    })

    app.get("/api/auth/profile", async (req, res) => {
        if (req.auth) {
            try {
                let r = await model.findOne({ _id: req.auth._id })
                r.lastLogin = Date.now()
                r.save()

                r = await model.findOne({ _id: req.auth._id })
                    .populate('group')
                res.json(r)
            } catch {
                res.json({ status: 'FAIL' })
            }
        } else {
            res.json({ status: 'FAIL' })
        }
    })

    // checagem de e-mail no banco forgot
    app.get('/api/signup/email/:email', async (req, res) => {
        let r = await model.findOne({ email: req.params.email, status: { $nin: ["blocked", "banned", "account-remove"] } })
        if (r) {
            res.json({ status: 'OK' })
        } else {
            res.json({ status: 'FAIL' })
        }
    })

    app.get('/api/auth/user', (req, res) => {
        res.json({ status: 'OK' })
    })
}