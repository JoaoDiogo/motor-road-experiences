require('dotenv').config()

import webpack from 'webpack'

import colors from 'vuetify/es5/util/colors'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - Motor Road Experiences',
    title: 'Motor Road Experiences',
    htmlAttrs: {
      lang: 'pt_BR'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    noscript: [
      { innerHTML: 'Esta Plataforma requer JavaScript.' }
    ]

  },

  // Customize the progress-bar color
  loading: { color: '#ed834a' },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    "~/css/main.css"
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/helpers',
    '~/plugins/vue-tooltip',
    '~/plugins/rules',
    '~/plugins/snackbar',
    '~/plugins/obj-components.js'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    ['@nuxtjs/moment'],
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '~/modules/auth',
    '@nuxtjs/pwa',
    '@nuxtjs/dotenv',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: process.env.API_URL,
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        },
        light: {
          primary: "#bf2600",
          secondary: "#03a9f4",
          accent: "#42a5f5",
          error: "#cf000f",
          warning: "#f0541e",
          info: "#607d8b",
          success: "#8bc34a"
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build

  build: {
    transpile: ['vue-tooltip'],
    /*
    ** You can extend webpack config here
    */
    extend (config, { isClient }) {
      if(process.env.NODE_ENV !== 'production' && isClient) {
        config.devtool = 'source-map'
      }
    },

    plugins: [
      new webpack.ProvidePlugin({
        '_': 'lodash'
      })
    ]

  },

  auth: {
    endpoints: {
      signin: '/auth/signin',
      profile: '/auth/profile',
      refresh: '/auth/refresh'
    },
    plugins: [
      { src: '~/plugins/socket.io', mode: 'client' },
    ]
  },

  moment: {
    locales: ['pt']
  },

  server: {
    port: 3000
  },
}
