export const state = () => ({
    notify: 0,
})

export const mutations = {
    SET_NOTIFY(state, value) {
        state.notify = value
    },

   
}

export const actions = {
    connect ({ commit }, socket, _id) {
        socket.on('on-line', payload => {
            console.log('on-line', payload)
        })

        socket.on('off-line', payload => {
            console.log('off-line', payload)
        })

        socket.on('notify', payload => {
            commit('SET_NOTIFY', payload.count)
        })

    }
}