export default class Auth {
    constructor ({ store, $axios }, options) {
        this.interval = null
        this.store = store
        this.$axios = $axios
        this.options = options

        if(!process.server) {
            if(this.store.state.auth.token) {
                $axios.setToken(this.store.state.auth.token)
                this.fetchUser()
                this.setRefresh()
            }
        }
    }

    async login (payload) {
        let { data } = await this.$axios.post(this.options.endpoints.signin, payload)

        if(data.token) {
            await this.setToken(data.token)
            await this.fetchUser()
            this.clearTimer()

            return true
        } else {
            return false
        }
    }

    async setRefresh() {
        if(!this.interval) {
            this.interval = setInterval(() => {
                if(this.logged) {
                    let auth = this.store.state.auth
                    if((((auth.exp - auth.iat) * 0.7) + auth.iat ) < (new Date().getTime() / 1000)) {
                        this.refresh()
                    }
                }
            }, 1000)
        }
    }

    async clearTimer () {
        if(this.interval) {
            clearInterval(this.interval),
            this.interval = null
        }
    }

    async logout () {
        this.clearTimer()
        this.setToken(null)
        this.store.commit('auth/set', {})
    }

    async refresh () {
        let { data } = await this.$axios.post(this.options.endpoints.refresh)
        if(data.token) {
            this.setToken(data.token)
            // this.fetchUser()
        }
    }

    async setToken (token) {
        this.$axios.setToken(`Bearer ${token}`)
        this.store.commit('auth/token', token)
    }

    async fetchUser () {
        let { data } = await this.$axios.get(this.options.endpoints.profile)
        this.store.commit('auth/set', data)
    }

    get logged () {
        let auth = this.store.state.auth
        return (auth.exp > (new Date().getTime() / 1000))
    }

    get profile () {
        return this.store.state.auth.profile
    }
}