export default options => ({
    namespaced: true,

    state: () => ({
        profile: {},
        token: '',
        iat: 0,
        exp: 0
    }),

    mutations: {
        set (state, value) {
            state.profile = value
        },

        token (state, value) {
            state.token = value

            // parse token
            if(value) {
                try {
                    let payload = value.split('.')[1]

                    if(process.server)
                        payload = JSON.parse(Buffer.from(payload, 'base64').toString())
                    else
                        payload = JSON.parse(atob(payload))

                    state.iat = payload.iat
                    state.exp = payload.exp
                } catch {
                    
                }
            } else {
                state.iat = 0
                state.exp = 0
            }

            if(!process.server)
                document.cookie = `__auth__.token=${value}; path=/`
        }
    },
    
    getters: {
        profile: state => state.profile,
        token: state => state.token
    }
})