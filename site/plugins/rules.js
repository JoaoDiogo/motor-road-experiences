const moment = require('moment')

export default ({ app }, inject) => {
    inject('rules', {
        required: v => !!v || 'Campo obrigatório',
        email: v => /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(v) || 'Informe um e-mail válido',
        date: v => moment(v, 'DD/MM/YYYY').isValid() || 'Informe uma data válida',
        cnpj: v => (c => {
            var b = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2]
            if (!c) return true
            if ((c = c.replace(/[^\d]/g, "")).length != 14) return false
            if (/0{14}/.test(c)) return false

            for (var i = 0, n = 0; i < 12; n += c[i] * b[++i]);
            if (c[12] != (((n %= 11) < 2) ? 0 : 11 - n)) return false

            for (var i = 0, n = 0; i <= 12; n += c[i] * b[i++]);
            if (c[13] != (((n %= 11) < 2) ? 0 : 11 - n)) return false

            return true
        })(v) || 'Informe um CNPJ válido',
        cpf: v => {
            function r(r) {
                for (var t = null, n = 0; 9 > n; ++n)
                    t += r.toString().charAt(n) * (10 - n);
                var i = t % 11;
                return i = 2 > i ? 0 : 11 - i
            }
            function t(r) {
                for (var t = null, n = 0; 10 > n; ++n)t += r.toString().charAt(n) * (11 - n);
                var i = t % 11; return i = 2 > i ? 0 : 11 - i
            }
            
            for (var a = v.replace(/\D/g, ""), u = a.substring(0, 9), f = a.substring(9, 11), v = 0; 10 > v; v++)
                if ("" + u + f == "" + v + v + v + v + v + v + v + v + v + v + v) return "CPF inválido"
            
            var c = r(u), e = t(u + "" + c)
            return f.toString() === c.toString() + e.toString() ? true : "CPF inválido"
        },
        numeric: v => parseInt(v) >= 0 || 'Campo necessita ser numérico',
        strong: v => {
            if (v) {
                if (v.length < 6)
                    return 'A senha deve conter no mínimo 6 caracteres'

                if (!/[a-z]/.test(v))
                    return 'A senha deve conter letras maiúsculas, minúsculas e números'

                if (!/[A-Z]/.test(v))
                    return 'A senha deve conter letras maiúsculas, minúsculas e números'

                if (!/[0-9]/.test(v))
                    return 'A senha deve conter letras maiúsculas, minúsculas e números'

                // if(!/[W+]/.test(v))
                //     return 'Senha deve conter caracteres especiais'
            } else {
                return false
            }

            return true
        },
        fullname: v => /^[A-ZÀ-Ÿ][A-zÀ-ÿ']+\s([A-zÀ-ÿ']\s?)*[A-ZÀ-Ÿ][A-zÀ-ÿ']+$/.test(v) || "Nome e sobrenome devem iniciar com letra maiúscula.",
        creditcard: v => {
            v = v.replace(/[- ]/g, '')
            console.log(v)
            let arr = Array()
            let total = 0
            let calc, calc2

            // Mastercard, Visa, Hiper, HiperCard, Diners, Elo
            for (let i = 0; i < v.length; i++) {
                if (i % 2 === 0) {
                    let dig = v[i] * 2
                    if (dig > 9) {
                        let dig1 = dig.toString().substr(0, 1)
                        let dig2 = dig.toString().substr(1, 1)
                        arr[i] = parseInt(dig1) + parseInt(dig2)
                    }
                    else {
                        arr[i] = parseInt(dig)
                    }

                    total += parseInt(arr[i])
                } else {
                    arr[i] = parseInt(v[i])
                    total += parseInt(arr[i])
                }
            }

            // Amex
            if(total % 10 !== 0) {
                total = 0
                for (let i = v.length; i > 0; i--) {
                    calc = parseInt(v.charAt(i - 1));
                    if(i % 2 === 0) {
                        calc2 = calc * 2;
                        if(calc2 > 9) calc2 -= 9
                        total += calc2;
                    } else {
                        total += calc;
                    }
                }
            }

            return total % 10 === 0 || "Numero de cartão inválido"
        }
    })
}
