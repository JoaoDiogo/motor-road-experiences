export default ({ app }, inject) => {
    /* inject('list', id => {
        return app.store.state.list.filter(i => i.id === id)[0]
    })

    inject('item', (item, id) => {
        return item.filter(i => i.id === id)[0]
    })
 */
    inject('debounce', (fn, delay) => {
        var timeout = null
        return function() {
            clearTimeout(timeout)
            var args = arguments
            var that = this
            timeout = setTimeout(function() {
                fn.apply(that, args)
            }, delay)
        }
    })
}