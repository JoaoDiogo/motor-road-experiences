import io from 'socket.io-client'

export default ({ $auth, store }, inject) => {
    const socket = io(process.env.WS_URL, { transports: ['polling'] })

    socket.on('register', () => {
        if($auth.profile._id)
            socket.emit('handshake', $auth.profile._id)
    })

    socket.on('match', () => {
        console.log('match')
        $auth.fetchUser()
    })

    inject('io', socket)

    store.dispatch('socket/connect', socket)
}