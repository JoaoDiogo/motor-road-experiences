export default ({ $auth, $axios }) => {
    setInterval(async function() {
        let { data } = await $axios.get('/auth/refresh')
        $axios.setToken(data.token)
        if(process.browser) {
            localStorage.setItem('auth._token.local', data.token)
        }
        if($auth) await $auth.setToken('local', data.token)
    }, 
    25 * 60 * 1000)
}