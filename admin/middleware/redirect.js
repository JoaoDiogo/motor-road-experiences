export default ({ $auth, redirect }) => {
    console.log($auth.profile)
    if(!$auth.logged) redirect('/login')
    if($auth.profile.role !== 'admin') redirect('/login')
}