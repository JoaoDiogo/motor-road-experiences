import Vue from 'vue'
import Snackbar from '@/components/Snackbar'
import Avatar from '@/components/Avatar'
//import { mask } from 'vue-the-mask'

Vue.component('GSnackbar', Snackbar)
Vue.component('GAvatar', Avatar)
//Vue.directive('mask', mask)