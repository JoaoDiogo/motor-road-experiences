export default ({ app }, inject ) => {
    inject('rules', {
        required: v => !!v || 'Campo obrigatório',
        email: v => /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(v) || 'Informe um e-mail válido',
        cnpj: v => (c => {
            var b = [6,5,4,3,2,9,8,7,6,5,4,3,2]
            if(!c) return true
            if((c = c.replace(/[^\d]/g,"")).length != 14) return false
            if(/0{14}/.test(c)) return false

            for (var i = 0, n = 0; i < 12; n += c[i] * b[++i]);
            if(c[12] != (((n %= 11) < 2) ? 0 : 11 - n)) return false

            for (var i = 0, n = 0; i <= 12; n += c[i] * b[i++]);
            if(c[13] != (((n %= 11) < 2) ? 0 : 11 - n)) return false

            return true
        })(v) || 'Informe um CNPJ válido',
        numeric: v => parseInt(v) >= 0 || 'Campo necessita ser numérico'
    })
}