import Vue from 'vue'

import wysiwyg from "vue-wysiwyg"
Vue.use(wysiwyg, {
    hideModules: {
        image: true,
        table: true,
        code: true
    },
    
    forcePlainTextOnPaste: true,
    maxHeight: "500px"
})

import "vue-wysiwyg/dist/vueWysiwyg.css"