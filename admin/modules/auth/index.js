import path from 'path'
import consola from 'consola'

export default function Auth (moduleOptions) {
    const options = Object.assign({}, this.options.auth, moduleOptions)
    options.namespace = 'auth'

    if(!this.options.store) {
        consola.fatal('Enable vuex store by creating `store/index.js`.')
    }

    function copy (files, options, register) {
        files.map(file => {
            const { dst } = this.addTemplate({
                src: path.resolve(__dirname, 'lib', file),
                fileName: path.join(options.namespace, file),
                options: options
            })

            if(register)
                this.options.plugins.push(path.resolve(this.options.buildDir, dst))
        })
    }

    // register plugins
    const plugins = [
        'plugin.js',
        'store.js',
        'middleware.js'
    ]

    // coppy files
    const files = [
        'auth.js'
    ]

    copy.call(this, plugins, options, true)
    copy.call(this, files, options)

    if (options.plugins) {
        options.plugins.forEach(p => this.options.plugins.push(p))
        delete options.plugins
    }
}