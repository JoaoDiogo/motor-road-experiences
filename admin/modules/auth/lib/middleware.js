import Middleware from '../middleware'

const options = JSON.parse(`<%= JSON.stringify(options) %>`)

// compatibilidade Edge - não suporte Object.fromEntries
function ObjectFromEntries(iter) {
    const obj = {};
  
    for (const pair of iter) {
      if (Object(pair) !== pair) {
        throw new TypeError('iterable for fromEntries should yield objects');
      }
  
      // Consistency with Map: contract is that entry has "0" and "1" keys, not
      // that it is an array or iterable.
  
      const { '0': key, '1': val } = pair;
  
      Object.defineProperty(obj, key, {
        configurable: true,
        enumerable: true,
        writable: true,
        value: val,
      });
    }
  
    return obj;
  }

Middleware[options.namespace] = async context => {
    const { $auth, $axios, req } = context

    if(!process.server) $auth.setRefresh()

    // parse cookies
    let cookies = {}
    if(process.server) {
        if(req.headers.cookie) {
            cookies = ObjectFromEntries(
                req.headers.cookie.split('; ').map(cookie => {
                    const p = cookie.split('=')
                    return p
                }
            ))
        }
    } else {
        if(document.cookie) {
            cookies = ObjectFromEntries(
                document.cookie.split('; ').map(cookie => {
                    const p = cookie.split('=')
                    return p
                }
            ))
        }
    }

    let token = cookies["__auth__.token"]
    if(token) {
        await $auth.setToken(token)
        await $auth.fetchUser()
    }

    if (!$auth.logged) {
        context.redirect('/login')
    }
}