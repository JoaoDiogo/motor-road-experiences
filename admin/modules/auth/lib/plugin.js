import Auth from './auth'
import Store from './store'

export default (context, inject) => {
    const options = JSON.parse(`<%= JSON.stringify(options) %>`)
    const { store } = context

    // register store
    store.registerModule(options.namespace, Store(options), {
        preserveState: Boolean(store.state[options.namespace])
    })

    const $auth = new Auth(context, options)
    inject('auth', $auth)
    context.$auth = $auth
}